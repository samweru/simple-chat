<?php

header("Content-type: application/json");

require "vendor/autoload.php";

use Respect\Rest\Router;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

$fs = new Filesystem();

$file = "chat.log";
if(!$fs->exists($file))
	$fs->touch($file);

$router = new Router("/chat");

$router->get("/push", function() use($file){

	$username = trim($_GET["username"]);
	$message = trim($_GET["message"]);

	try{

		if(!empty($username) && !empty($message))
			$chatline = json_encode(array("user"=>$username, "message"=>$message));
		else
			throw new Exception("Error ocurred!");

		file_put_contents($file, $chatline.PHP_EOL, FILE_APPEND);

		return json_encode(array("code"=>1, "msg"=>"Message sent successfully."));
	}
	catch(Exception $e){

		return json_encode(array("code"=>0, "msg"=>"Failed to send message"));
	}
});

$router->get("/pull", function() use($file){

	return sprintf("[%s]", implode(",", file($file)));
});

print $router->run();